# Minato CI

## Usage

include this file on your ci.yml

### Rails container

```
include:
  - remote: https://gitlab.com/ferreri/minato/minato-ci/-/raw/v2.1.0/presets/rails_container.gitlab-ci.yml
```

### Rails image

```
include:
  - remote: https://gitlab.com/ferreri/minato/minato-ci/-/raw/v2.1.0/presets/rails_image.gitlab-ci.yml
```

### Rails application

```
include:
  - remote: https://gitlab.com/ferreri/minato/minato-ci/-/raw/v2.1.0/presets/rails.gitlab-ci.yml
```

### React

```
include:
  - remote: https://gitlab.com/ferreri/minato/minato-ci/-/raw/v2.1.0/presets/react.gitlab-ci.yml
```

### React Image

```
include:
  - remote: https://gitlab.com/ferreri/minato/minato-ci/-/raw/v2.1.0/presets/react_image.gitlab-ci.yml
```

### Container

basic container build and release steps

```
include:
  - remote: https://gitlab.com/ferreri/minato/minato-ci/-/raw/v2.1.0/presets/container.gitlab-ci.yml
```

### Hydra

```
include:
  - remote: https://gitlab.com/ferreri/minato/minato-ci/-/raw/v2.1.0/presets/hydra.gitlab-ci.yml
```

### Kratos

```
include:

  - remote: https://gitlab.com/ferreri/minato/minato-ci/-/raw/v2.1.0/presets/kratos.gitlab-ci.yml

```

### Web (JS) Libs

```
include:

  - remote: https://gitlab.com/ferreri/minato/minato-ci/-/raw/v2.1.0/presets/js_lib.gitlab-ci.yml

```